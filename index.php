<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/styles/materialize.min.css">
    <link rel="stylesheet" href="assets/styles/styles.css">
    <link rel="stylesheet" href="assets/styles/dataTables.css">
    <link rel="stylesheet" href="assets/styles/responsive.dataTables.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <title>INDEX</title>
</head>
<body>
<nav>
    <div class="nav-wrapper">
      <a href="#!" class="brand-logo">MVC_CURSO</a>
      <a href="#" data-target="opciones" class="sidenav-trigger"><i class="material-icons">menu</i></a>
      <ul class="right hide-on-med-and-down">
        <li><a href="Views/ViewAspirante.php">Aspirantes</a></li>
        <li><a href="Views/ViewCatalogo_cursos.php">Cursos</a></li>
        <li><a href="Views/ViewAspirante_curso.php">Aspirantes-Cursos</a></li>
      </ul>
    </div>
  </nav>
    
  <ul class="sidenav" id="opciones">
    <li><a href="Views/ViewAspirante.php">Aspirantes</a></li>
    <li><a href="Views/ViewCatalogo_cursos.php">Cursos</a></li>
    <li><a href="Views/ViewAspirante_curso.php">Aspirantes-Cursos</a></li>
  </ul>

  <main>
    <div class= "container">
        <div class= "row center">
        <h1>BIENVENIDOS:D</h1>
        <img src="assets/images/welcome.jpg" alt="" class="responsive-img">
        </div>
    </div>
  </main>

    <?php require "Views/footer.php";?>
    <script src="assets/scripts/jquery.min.js"></script>
    <script src="assets/scripts/dataTables.js"></script>
    <script src="assets/scripts/sweetalert2.js"></script>
    <script src="assets/scripts/materialize.min.js"></script>
    <script src="assets/scripts/scripts.js"></script>
    <script src="assets/scripts/dataTables.responsive.min.js"></script>
</body>
</html>