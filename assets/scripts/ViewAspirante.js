$(document).ready(function () {
    let flag;
    /**
     * DATATABLES ESPAÑOL
     */
    let languaje= {
        "sProcessing": "Procesando ...",
        "sLengthMenu": "",
        "sZeroRecords": "Ningun resultado encontrado",
        "sEmptyTable": "No hay datos en la tabla",
        "sInfo": "Líneas _START_ a _END_ de _TOTAL_ Registros",
        "sInfoEmpty": "No se muestra ninguna línea",
        "sInfoFiltered": "(Filtrar maximo _MAX_)",
        "sInfoPostFix": "",
        "sSearch": "Buscar",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
        "sFirst": "Primero", "sLast": "Ultimo", "sNext": "Siguiente", "sPrevious": "Previo"
        }
    }
    /**
     * TABLA INICIAL CARGA TODOS LOS DATOS REGISTRADOS
     */
    let TablaAspirantes =  $('#tabla-aspirantes').DataTable({
        //Definicion de lenguaje
        language: languaje,
        //Responsividad
        responsive: true,
        //Carga de datos en el controlador
        ajax:{
            url:"../Controllers/AspiranteController.php",
            type:"POST",
            dataType:"JSON",
            //SIEMPRE CARGARA LOS DATOS DEl FLAG 1
            data:{"flag":1},
            dataSrc:"data",
        },
        columns: [
            {"data":"RFC"},
            {"data":"NOMBRE"},
            {"data":"PATERNO"},
            {"data":"MATERNO"},
            {"data":"EMPRESA"},
            {"data":"TELEFONO"},
            {"data":"EMAIL"},
            {"data":"FECHA_REGISTRO"},
            {"defaultContent": 
                `<a  href="#" class="btn red btnBorrar"><i class="material-icons">cancel</i></a>
                 <a  href="#" class="btn green btnEditar"><i class="material-icons">update</i></a>
                `
            },
        ]
    })

    /**
     * FUNCION BOTON DE BORRAR REGISTRO ASPIRANTE
     */
    $(document).on("click",".btnBorrar", function () {
        flag = 2
        fila = $(this).closest("tr");
        rfc = fila.find('td:eq(0)').text();
        $.ajax({
            type: "POST",
            url: "../Controllers/AspiranteController.php",
            data: {
                flag:flag,
                rfc: rfc
            },
            success: function (response) {
                Swal.fire(
                    'Exito',
                    response,
                    'success'
                );
                TablaAspirantes.ajax.reload()
            }
        });
    });

    /**
     * FUNCION BOTON DE ACTUALIZAR ASPIRANTE
     */
    $(document).on("click",".btnEditar", function () {
        flag = 3;
        fila = $(this).closest("tr");
        rfc = fila.find('td:eq(0)').text();
        nombre = fila.find('td:eq(1)').text();
        paterno = fila.find('td:eq(2)').text();
        materno = fila.find('td:eq(3)').text();
        empresa = fila.find('td:eq(4)').text();
        telefono= fila.find('td:eq(5)').text();
        email = fila.find('td:eq(6)').text();
        $("#iRFC").val(rfc);
        $("#iNombre").val(nombre);
        $("#iPaterno").val(paterno);
        $("#iMaterno").val(materno);
        $("#iEmpresa").val(empresa);
        $("#iTelefono").val(telefono);
        $("#iEmail").val(email);
        $('#modal-aspirantes').modal('open');
    });
    
    /**
     * FUNCION BOTON AGREGAR ASPIRANTE
     */
    $("#btnAgregar").click(function (e) { 
        e.preventDefault();
        flag = 4;
        $('#modal-aspirantes').modal('open');
        $("#form-aspirantes").trigger("reset");
    });

    /**
     * SUBMIT MODAL
     */
    $("#form-aspirantes").submit(function (e) {
        e.preventDefault();
        //ENVIO DE FORMULARIO, VERIFICA SI FLAG = 4 PARA INSERTAR
            $.ajax({
                type: "POST",
                url: "../Controllers/AspiranteController.php",
                data: {
                    flag:flag,
                    rfc: $("#iRFC").val(),
                    nombre: $("#iNombre").val(),
                    paterno: $("#iPaterno").val(),
                    materno: $("#iMaterno").val(),
                    empresa: $("#iEmpresa").val(),
                    telefono: $("#iTelefono").val(),
                    email: $("#iEmail").val()
                },
                success: function (response) {
                    $('#modal-aspirantes').modal('close');
                    TablaAspirantes.ajax.reload();
                    Swal.fire(
                        'Exito',
                        response,
                        'success'
                    ); 
                }
            });
    });
});