$(document).ready(function () {
    let flag;
    /**
     * DATATABLES ESPAÑOL
     */
    let languaje= {
        "sProcessing": "Procesando ...",
        "sLengthMenu": "",
        "sZeroRecords": "Ningun resultado encontrado",
        "sEmptyTable": "No hay datos en la tabla",
        "sInfo": "Líneas _START_ a _END_ de _TOTAL_ Cursos",
        "sInfoEmpty": "No se muestra ninguna línea",
        "sInfoFiltered": "(Filtrar maximo _MAX_)",
        "sInfoPostFix": "",
        "sSearch": "Buscar",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
        "sFirst": "Primero", "sLast": "Ultimo", "sNext": "Siguiente", "sPrevious": "Previo"
        }
    }
    /**
     * TABLA INICIAL CARGA TODOS    */
    let TablaCursos =  $('#tabla-cursos').DataTable({
        //Definicion de lenguaje
        language: languaje,
        //Carga de datos en el controlador
        ajax:{
            url:"../Controllers/CursoController.php",
            type:"POST",
            dataType:"JSON",
            //SIEMPRE CARGARA LOS DATOS DEl FLAG 1
            data:{"flag":1},
            dataSrc:"data",
        },
        columns: [
            {"data":"ID_CURSO"},
            {"data":"NOMBRE_CURSO"},
            {"data":"FECHA_ALTA"},
            {"defaultContent": 
                `<a  href="#" class="btn red btnBorra"><i class="material-icons">cancel</i></a>
                 <a  href="#" class="btn green btnEdita"><i class="material-icons">update</i></a>
                `
            },
        ]
    })

    /**
     * FUNCION BOTON DE BORRAR 
     */
    $(document).on("click",".btnBorra", function () {
        flag = 2
        fila = $(this).closest("tr");
        id = fila.find('td:eq(0)').text();
        $.ajax({
            type: "POST",
            url: "../Controllers/CursoController.php",
            data: {
                flag:flag,
                id: id
            },
            success: function (response) {
                Swal.fire(
                    'Exito',
                    response,
                    'success'
                );
                TablaCursos.ajax.reload()
            }
        });
    });

    /**
     * FUNCION BOTON DE ACTUALIZAR 
     */
    $(document).on("click",".btnEdita", function () {
        flag = 3;
        fila = $(this).closest("tr");
        id = fila.find('td:eq(0)').text();
        nombre = fila.find('td:eq(1)').text();
        $("#inombre").val(nombre);
        $('#modal-cursos').modal('open');
    });
    
    /**
     * FUNCION BOTON AGREGAR 
     */
    $("#btnAgrega").click(function (e) { 
        e.preventDefault();
        flag = 4;
        $('#modal-cursos').modal('open');
        $("#form-cursos").trigger("reset");
    });

    /**
     * SUBMIT MODAL
     */
    $("#form-cursos").submit(function (e) {
        e.preventDefault();
        //ENVIO DE FORMULARIO, VERIFICA SI FLAG = 4 PARA INSERTAR
            $.ajax({
                type: "POST",
                url: "../Controllers/CursoController.php",
                data: {
                    flag:flag,
                    id: $("#id").val(),
                    nombre: $("#inombre").val()
                },
                success: function (response) {
                    $('#modal-cursos').modal('close');
                    TablaCursos.ajax.reload();
                    Swal.fire(
                        'Exito',
                        response,
                        'success'
                    ); 
                }
            });
    });
});