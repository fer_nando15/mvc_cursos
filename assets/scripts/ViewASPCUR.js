$(document).ready(function () {
    let flag;
    let old_curso= "";
    let old_rfc="";
    /**
     * DATATABLES ESPAÑOL
     */
    let languaje= {
        "sProcessing": "Procesando ...",
        "sLengthMenu": "",
        "sZeroRecords": "Ningun resultado encontrado",
        "sEmptyTable": "No hay datos en la tabla",
        "sInfo": "Líneas _START_ a _END_ de _TOTAL_ Cursos",
        "sInfoEmpty": "No se muestra ninguna línea",
        "sInfoFiltered": "(Filtrar maximo _MAX_)",
        "sInfoPostFix": "",
        "sSearch": "Buscar",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
        "sFirst": "Primero", "sLast": "Ultimo", "sNext": "Siguiente", "sPrevious": "Previo"
        }
    }
    /**
     * TABLA INICIAL CARGA TODOS    */
    let Tabla_ASPCUR =  $('#tabla-aspirante-curso').DataTable({
        //Definicion de lenguaje
        language: languaje,
        //Carga de datos en el controlador
        ajax:{
            url:"../Controllers/Aspirante_CursoController.php",
            type:"POST",
            dataType:"JSON",
            //SIEMPRE CARGARA LOS DATOS DEl FLAG 1
            data:{"flag":1},
            dataSrc:"data",
        },
        columns: [
            {"data":"RFC"},
            {"data":"ID_CURSO"},
            {"defaultContent": 
                `<a  href="#" class="btn red btnBorrar"><i class="material-icons">cancel</i></a>
                 <a  href="#" class="btn green btnEditar"><i class="material-icons">update</i></a>
                `
            },
        ]
    })

    /**
     * FUNCION BOTON DE BORRAR 
     */
    $(document).on("click",".btnBorrar", function () {
        flag = 2
        fila = $(this).closest("tr");
        iRFC = String(fila.find('td:eq(0)').text());
        iCurso = String(fila.find('td:eq(1)').text());
        console.log(iCurso);
        $.ajax({
            type: "POST",
            url: "../Controllers/Aspirante_CursoController.php",
            data: {
                flag:flag,
                iRFC: iRFC,
                iCurso: iCurso
            },
            success: function (response) {
                Swal.fire(
                    'Exito',
                    response,
                    'success'
                );
                Tabla_ASPCUR.ajax.reload()
            }
        });
    });

    /**
     * FUNCION BOTON DE ACTUALIZAR 
     */
    $(document).on("click",".btnEditar", function () {
        flag = 3;
        fila = $(this).closest("tr");
        old_rfc = fila.find('td:eq(0)').text();
        old_curso = fila.find('td:eq(1)').text();
        icurso = fila.find('td:eq(1)').text();
        $("#icurso").val(icurso);
        $('#modal-aspirante-curso').modal('open');
        $( "#iRFC" ).prop( "disabled", true )
        $("#iRFC").val(old_rfc);
    });
    
    /**
     * FUNCION BOTON AGREGAR 
     */
    $("#btnAgregar").click(function (e) { 
        e.preventDefault();
        flag = 4;
        $('#modal-aspirante-curso').modal('open');
        $("#form-aspirante-curso").trigger("reset");
    });

    /**
     * SUBMIT MODAL
     */
    $("#form-aspirante-curso").submit(function (e) {
        e.preventDefault();
        $( "#iRFC" ).prop( "disabled", false )
        if (flag == 3){
            data2 = {
                flag: flag,
                rfc: old_rfc,
                iCursoViejo: old_curso,
                curso: $("#iCurso").val()
            }
        }
        else{
            data2 = {
                flag:flag,
                rfc: $("#iRFC").val(),
                curso: $("#iCurso").val()
            }
        }

        //ENVIO DE FORMULARIO, VERIFICA SI FLAG = 4 PARA INSERTAR
            $.ajax({
                type: "POST",
                url: "../Controllers/Aspirante_CursoController.php",
                data: data2,
                success: function (response) {
                    $('#modal-aspirante-curso').modal('close');
                    Tabla_ASPCUR.ajax.reload();
                    Swal.fire(
                        'Exito',
                        response,
                        'success'
                    ); 
                }
            });
    });
});