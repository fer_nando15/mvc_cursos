<?php
require "../Models/Catalogo_CursoModel.php";
$flag = $_POST["flag"];
$curso = new Catalogo_CursoModel();
switch($flag){
    /**LISTADO DE CURSOS */
    case 1:
        $resultado = $curso->getAllCursos();
        echo $resultado;
        break;

    /** BORRA CURSO */
    case 2:
        $id = $_POST["id"];
        $resultado = $curso->delete_curso($id);
        echo $resultado;
        break;
    
        /**ACTUALIZA CURSO */
    case 3:
        $id = $_POST["id"];
        $nombre = $_POST["nombre"];
        $resultado = $curso->updateCurso($id,$nombre);
        echo $resultado;
        break;
    
        //**INSERTAR CURSO */
    case 4:
        $id = $_POST["id"];
        $nombre = $_POST["nombre"];
        $resultado = $curso->insert_curso($id,$nombre);
        echo $resultado;
        break;
}
?>