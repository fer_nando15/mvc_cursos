<?php
require "../Models/AspirantesModel.php";
$flag = $_POST["flag"];
$aspirante = new AspirantesModel();
switch ($flag) {
    /**
     * DEVUELVE TODOS LOS REGISTROS
     */
    case 1:
        $resultado = $aspirante->getAllAspirantes();
        echo $resultado;
        break;
    /**
     * BORRA UN REGISTRO
     */
    case 2:
        $rfc = $_POST["rfc"];
        $resultado = $aspirante->deleteAspirante($rfc);
        echo $resultado;
        break;
    /**
     * ACTUALIZA UN REGISTRO
     */
    case 3:
        $rfc = $_POST["rfc"];
        $nombre = $_POST["nombre"];
        $paterno = $_POST["paterno"];
        $materno = $_POST["materno"];
        $empresa = $_POST["empresa"];
        $telefono = $_POST["telefono"];
        $email = $_POST["email"];
        $resultado = $aspirante->updateAspirante($rfc,$nombre, $paterno, $materno, $empresa, $telefono,$email);
        echo $resultado;
        break;
    /**
     * CREA UN REGISTRO
     */
    case 4:
        $rfc = $_POST["rfc"];
        $nombre = $_POST["nombre"];
        $paterno = $_POST["paterno"];
        $materno = $_POST["materno"];
        $empresa = $_POST["empresa"];
        $telefono = $_POST["telefono"];
        $email = $_POST["email"];
        $resultado = $aspirante->setAspirante($rfc,$nombre, $paterno, $materno, $empresa, $telefono,$email);
        echo $resultado;
        break;

}
?>