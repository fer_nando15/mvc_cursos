<?php
    require "../Models/Aspirantes_CursoModel.php";
    $flag = $_POST["flag"];
    $curso = new Aspirantes_CursoModel();
    switch($flag){
        /**LISTADO DE Aspirantes-CURSOS */
        case 1:
            $resultado = $curso->getAllAspirantes_Curso();
            echo $resultado;
            break;

        /** BORRAR ASPIRANTES-CURSO */
        case 2:
            $rfc = $_POST["iRFC"];
            $icurso = $_POST["iCurso"];
            $resultado = $curso->deleteAspirante_Curso($rfc,$icurso);
            echo $resultado;
            break;
        
            /**ACTUALIZA ASPIRANTE-CURSO */
        case 3:
            $RFC = $_POST["rfc"];
            $CURSO_VIEJO = $_POST["iCursoViejo"];
            $CURSO_NUEVO = $_POST["curso"];
            $resultado = $curso->updateAspirante_Curso($CURSO_VIEJO, $CURSO_NUEVO, $RFC);

            echo $resultado;
            break;
        
            //**INSERTAR ASPIRANTE-CURSO */
        case 4:
            $RFC = $_POST["rfc"];
            $CURSO = $_POST["curso"];
            $resultado = $curso->setAspirante_Curso($RFC,$CURSO);
            echo $resultado;
            break;
    }
?>