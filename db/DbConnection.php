<?php
require "config.php";
class DbConnection{

     /**
      * CONEXION A LA BASE DE DATOS
      */
    public function __construct(){
        $this->conn = new mysqli(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        $this->conn->set_charset('utf8');
        if (!$this->conn){
            echo "ERROR EN LA CONEXION";
        }
    }

    /**
     * CIERRE DE LA CONEXION
     */
    public function __destruct()
    {
        mysqli_close($this->conn);
    }
}
?>