<?php
require "../db/DbConnection.php";
date_default_timezone_set("America/Monterrey");
class AspirantesModel extends DbConnection{
    
    /**
     * CONSTRUCTOR CREA ARREGLO DE DATOS
     */
    public function __construct()
    {
        parent::__construct();
        $this->Aspirantes = array();
    }

    /**
     * METODO PARA MOSTRAR TODOS LOS REGISTROS DE ASPIRANTES
     */
    public function getAllAspirantes(){
        $sql = "SELECT * FROM ASPIRANTES";
        $resultado = mysqli_query($this->conn,$sql) or die (mysqli_error($this->conn));
        if (mysqli_num_rows($resultado)>0){
            while($row = $resultado->fetch_assoc()) {
                $this->Aspirantes["data"][]= $row;
            }
            return json_encode($this->Aspirantes);
        }
        else{
            $this->Aspirantes= array("data"=>[]);
            return json_encode($this->Aspirantes);
        } 
    }

    /**
     * METODO PARA MOSTRAR DATOS  
     */
    public function getAspirante($rfc)
    {
        $rfc = $this->conn->real_escape_string($rfc);
        $sql = "SELECT * FROM ASPIRANTES WHERE RFC = '$rfc'";
        $resultado = mysqli_query($this->conn,$sql) or die (mysqli_error($this->conn));
        if (mysqli_num_rows($resultado)>0){
            while($row = $resultado->fetch_assoc()) {
                $this->Aspirantes[]=$row;
            }
            return json_encode($this->Aspirantes);
        }
        else{
            $this->Aspirantes= array("data"=>[]);
            return json_encode($this->Aspirantes);
        }
    }
    
    /**
     *METODO PARA INSERTAR DATOS 
     */
    public function setAspirante($rfc, $nombre, $paterno, $materno, $empresa, $telefono, $email)
    {
        $rfc = $this->conn->real_escape_string($rfc);
        $nombre = $this->conn->real_escape_string($nombre);
        $paterno = $this->conn->real_escape_string($paterno);
        $materno = $this->conn->real_escape_string($materno);
        $empresa = $this->conn->real_escape_string($empresa);
        $telefono = $this->conn->real_escape_string($telefono);
        $email = $this->conn->real_escape_string($email);
        $fecha_registro = date("Y-m-d H:i:s");
        $sql = "INSERT INTO ASPIRANTES (RFC, NOMBRE, PATERNO, MATERNO, EMPRESA, TELEFONO, EMAIL, FECHA_REGISTRO)
            VALUES ('$rfc','$nombre','$paterno', '$materno', '$empresa', $telefono, '$email','$fecha_registro')";
        $resultado = mysqli_query($this->conn,$sql) or die (mysqli_error($this->conn));
        if(mysqli_affected_rows($this->conn)){
            return "Exito al insertar";
        }
    }

    /**
     * METODO PARA BORRAR
     */
    public function deleteAspirante($rfc)
    {
        $rfc = $this->conn->real_escape_string($rfc);
        $sql = "DELETE FROM ASPIRANTES WHERE RFC = '$rfc'";
        $resultado = mysqli_query($this->conn,$sql) or die (mysqli_error($this->conn));
        if($resultado){
            return "Exito al borrar el registro $rfc";
        }
    }

    /**
     * METODO PARA ACTUALIZAR
     */
    public function updateAspirante($rfc, $nombre, $paterno, $materno, $empresa, $telefono, $email)
    {
        $rfc = $this->conn->real_escape_string($rfc);
        $nombre = $this->conn->real_escape_string($nombre);
        $paterno = $this->conn->real_escape_string($paterno);
        $materno = $this->conn->real_escape_string($materno);
        $empresa = $this->conn->real_escape_string($empresa);
        $telefono = $this->conn->real_escape_string($telefono);
        $email = $this->conn->real_escape_string($email);
        $sql = "UPDATE ASPIRANTES SET NOMBRE = '$nombre',PATERNO = '$paterno',MATERNO = '$materno',EMPRESA = '$empresa',TELEFONO = '$telefono',EMAIL = '$email' WHERE RFC = '$rfc'";
        $resultado = mysqli_query($this->conn,$sql) or die (mysqli_error($this->conn));
        if($resultado){
            return "Exito al actualizar el registro $rfc";
        }
    }
}
?>