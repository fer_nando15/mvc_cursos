<?php
require "../db/DbConnection.php";
date_default_timezone_set("America/Monterrey");
class Catalogo_CursoModel extends DbConnection{

    public function __construct()
    {
        parent:: __construct();
        $this->Catalogo_curso = array();

    }
    /**
     * METODO LISTAR CURSOS 
     */
    public function getAllCursos(){
        $sql = "SELECT * FROM CATALOGO_CURSO";
        $resultado = mysqli_query($this->conn,$sql) or die (mysqli_error($this->conn));
        if (mysqli_num_rows($resultado)>0){
            while($row = $resultado->fetch_assoc()) {
                $this->Catalogo_curso["data"][]=$row;
            }
            return json_encode($this->Catalogo_curso);
        }
        else{
            $this->Catalogo_curso= array("data"=>[]);
            return json_encode($this->Catalogo_curso);
        } 
    }


        /** 
        * METODO CURSOS POR ID
        */
    public function getCurso($id)
    {
        $id = $this->conn->real_escape_string($id);
        $sql = "SELECT * FROM CATALOGO_CURSO WHERE ID_CURSO = '$id'";
        $resultado = mysqli_query($this->conn,$sql) or die (mysqli_error($this->conn));
        if (mysqli_num_rows($resultado)>0){
            while($row = $resultado->fetch_assoc()) {
                $this->Catalogo_curso[]=$row;
            }
            return json_encode($this->Catalogo_curso);
        }
        else{
            $this->Catalogo_curso= array("data"=>[]);
            return json_encode($this->Catalogo_curso);
        }
    }
    /**METODO INSERT CURSO */

    public function insert_curso($id, $nombre)
    {
        $id = $this->conn->real_escape_string($id);
        $nombre = $this->conn->real_escape_string($nombre);
        $fecha_alta = date("Y-m-d");
        $sql = "INSERT INTO CATALOGO_CURSO (ID_CURSO, NOMBRE_CURSO,FECHA_ALTA)
            VALUES ('$id','$nombre','$fecha_alta')";
        $resultado = mysqli_query($this->conn,$sql) or die (mysqli_error($this->conn));
        if(mysqli_affected_rows($this->conn)){
            return "Exito al insertar";
        }
    }

    /**METODO UPDATE CURSO */
    public function updateCurso($id, $nombre)
    {
        $id = $this->conn->real_escape_string($id);
        $nombre = $this->conn->real_escape_string($nombre);
        $sql = "UPDATE CATALOGO_CURSO SET NOMBRE_CURSO = '$nombre' WHERE ID_CURSO = '$id'";
        $resultado = mysqli_query($this->conn,$sql) or die (mysqli_error($this->conn));
        if($resultado){
            return "EXITO AL ACTUALIZAR EL CURSO $id";
        }
    }

    /**METODO DELETE CURRSO */
    public function delete_curso($id){
        $id = $this->conn->real_escape_string($id);
        $sql = "DELETE FROM CATALOGO_CURSO WHERE ID_CURSO = '$id'";
        $resultado=mysqli_query($this->conn,$sql) or die (mysqli_error($this->conn));
        if($resultado){
            return "EXITO AL BORRAR EL CURSO $id";
        }
    }
}
?> 