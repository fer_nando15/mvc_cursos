<?php
    require "../db/DbConnection.php";
    date_default_timezone_set("America/Monterrey"); #Cambiando el uso horario

    /**
     * definicion de la clase 
    */
    class Aspirantes_CursoModel extends DbConnection{

        /**
         * Constructor 
        */
        public function __construct(){
            parent::__construct();
            $this->Aspirantes_Cursos = array();
        }

        /**
         * METODO PARA MOSTRAR TODOS LOS REGISTROS DE ASPIRANTES
         */
        public function getAllAspirantes_Curso(){
            $sql = "SELECT * FROM ASPIRANTES_CURSOS";
            $resultado = mysqli_query($this->conn,$sql) or die (mysqli_error($this->conn));
            if (mysqli_num_rows($resultado)>0){
                while($row = $resultado->fetch_assoc()) {
                    $this->Aspirantes_Cursos["data"][]=$row;
                }
                return json_encode($this->Aspirantes_Cursos);
            }
            else{
                $this->Aspirantes= array("data"=>[]);
                return json_encode($this->Aspirantes);
            } 
        }
        /**
         * METODO PARA SELECCIONAR TODOS LOS CURSOS DE UN ASPIRANTE
         */
        public function getAspirante_Curso_Aspirante($rfc){
            //$rfc = $this->conn->real_escape_string($rfc);
            $sql = "SELECT * FROM ASPIRANTES_CURSOS WHERE RFC = '$rfc'";
            $resultado = mysqli_query($this->conn,$sql) or die (mysqli_error($this->conn));
            if (mysqli_num_rows($resultado)>0){
                while($row = $resultado->fetch_assoc()) {
                    $this->Aspirantes_Cursos["data"][]=$row;
                }
                return json_encode($this->Aspirantes_Cursos);
            }
            else{
                return $this->Aspirantes= array("data"=>[]);
                return json_encode($this->Aspirantes);
            }
        }
        /**
         * METODO PARA SELECCIONAR TODOS LOS ASPIRANTES DE UN CURSO
         */
        public function getAspirante_Curso_Curso($id){
            $id = $this->conn->real_escape_string($id);
            $sql = "SELECT * FROM ASPIRANTES_CURSOS WHERE ID_CURSO = '$id'";
            $resultado = mysqli_query($this->conn,$sql) or die (mysqli_error($this->conn));
            if (mysqli_num_rows($resultado)>0){
                while($row = $resultado->fetch_assoc()) {
                    $this->Aspirantes_Cursos[]=$row;
                }
                return $this->Aspirantes_Cursos;
            }
            else{
                return "EL CURSO $id NO SE ENCUENTRA REGISTRADO";
            }
        }
        /**
         * METODO PARA INSERTAR UN CURSO A UN ASPIRANTE O UN ASPIRANTE A UN CURSO
         */
        public function setAspirante_Curso($rfc, $id_curso){
            $rfc = $this->conn->real_escape_string($rfc);
            $id_curso = $this->conn->real_escape_string($id_curso);
            $sql = "INSERT INTO ASPIRANTES_CURSOS (RFC, ID_CURSO) VALUES ('$rfc','$id_curso')";
            $resultado = mysqli_query($this->conn,$sql) or die (mysqli_error($this->conn));
            if(mysqli_affected_rows($this->conn)){
                return "Exito al insertar";
            }
        }

        /**
         * METODO PARA BORRAR POR RFC Y CURSO
         */
        public function deleteAspirante_Curso($rfc,$curso){
            $rfc = "$rfc";
            $curso="$curso";
            $rfc = $this->conn->real_escape_string($rfc);
            $curso = $this ->conn ->real_escape_string(strval($curso));
            $sql = "DELETE FROM ASPIRANTES_CURSOS WHERE RFC = '$rfc' AND ID_CURSO = '$curso'";
            $resultado = mysqli_query($this->conn,$sql) or die (mysqli_error($this->conn));
            if($resultado){
                return "Exito al borrar el registro $rfc / $curso";
            }
        }

        /**
         * METODO PARA BORRAR POR RFC
         */
        public function deleteAspirante_Curso_RFC($rfc){
            $rfc = $this->conn->real_escape_string($rfc);
            $sql = "DELETE FROM ASPIRANTES_CURSOS WHERE RFC = '$rfc'";
            $resultado = mysqli_query($this->conn,$sql) or die (mysqli_error($this->conn));
            if($resultado){
                return "Exito al borrar el registro $rfc";
            }
        }

        /**
         * METODO PARA BORRAR POR CURSO
         */
        public function deleteAspirante_Curso_Curso($curso){
            $rfc = $this->conn->real_escape_string($curso);
            $sql = "DELETE FROM ASPIRANTES_CURSOS WHERE ID_CURSO = '$curso'";
            $resultado = mysqli_query($this->conn,$sql) or die (mysqli_error($this->conn));
            if($resultado){
                return "Exito al borrar el registro $curso";
            }
        }

        /**
         * METODO PARA ACTUALIZAR POR RFC Y ID_CURSO
         */
        public function updateAspirante_Curso($curso_viejo, $curso_nuevo, $rfc){
            $rfc = $this->conn->real_escape_string($rfc);
            $curso_nuevo = $this->conn->real_escape_string($curso_nuevo);
            $curso_viejo = $this->conn->real_escape_string($curso_viejo);
            $sql = "UPDATE ASPIRANTES_CURSOS SET ID_CURSO = $curso_nuevo WHERE ID_CURSO = $curso_viejo AND RFC = '$rfc'";
            $resultado = mysqli_query($this->conn,$sql) or die (mysqli_error($this->conn));
            if($resultado){
                return "Exito al actualizar el registro.";
            }
        }
    }
?>
