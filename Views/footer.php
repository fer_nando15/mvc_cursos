<footer class= "flow-text">
        <div class= "row">
            <div class = "col s12">
                <h5 class="center">Integrantes</h5>
                <div class ="divider"></div>
            </div>
        </div>
        <div class= "row">
            <div class = "col s12 m4 center">
                <h6>Emilio Barrera González</h6>
            </div>
            <div class = "col s12 m4 center">
                <h6>Luis Fernando García Morales</h6>
            </div>
            <div class = "col s12 m4 center">
                <h6>Angela Gabriela Sanchez Niño</h6>
            </div>
        </div>

</footer>