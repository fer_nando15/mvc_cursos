<!DOCTYPE html>
<html lang="en">
<head>
    <?php require "head.php" ?>
    <title>Document</title>
</head>
<body>

    <?require "header.php"?>
    <!-- Modal Structure -->
    <div id="modal-cursos" class="modal">
            <form id="form-cursos">
                <div class="modal-content">
                    <h6>CURSOS</h6>
                    <div class="row">
                        <div class="col s4">
                            <span>ID CURSO</span>
                            <input id="id"type="text" required >
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s4">
                            <span>NOMBRE</span>
                            <input id="inombre" type="text" required >
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button href="#" id="enviar" type="submit" class="waves-effect waves-green btn-flat">Enviar</button>
                    <button href="#" id="cancelar" class="modal-close waves-effect waves-green btn-flat">Cancelar</button>
                </div>
            </form>
        </div>
    <main>
        <div class="col s12">
            <button id="btnAgrega" class="right blue white-text waves-effect waves-teal btn-flat">Agregar Curso</button>
            <table id="tabla-cursos" style="width:100%">
                <thead>
                    <tr>
                        <th>ID CURSO</th>
                        <th>NOMBRE</th>
                        <th>FECHA ALTA</th>
                        <th>ACCIONES</th>
                    </tr>
                </thead>
            </table>
        </div>
    </main>
    <?php require "footer.php"?>
    <?php require "scripts.php"?>
    <script src="../assets/scripts/ViewCurso.js"></script>
</body>
</html>