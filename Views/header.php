  <nav>
    <div class="nav-wrapper">
      <a href="#!" class="brand-logo">MVC_CURSO</a>
      <a href="#" data-target="opciones" class="sidenav-trigger"><i class="material-icons">menu</i></a>
      <ul class="right hide-on-med-and-down">
        <li><a href="ViewAspirante.php">Aspirantes</a></li>
        <li><a href="ViewCatalogo_cursos.php">Cursos</a></li>
        <li><a href="ViewAspirante_curso.php">Aspirantes-Cursos</a></li>
      </ul>
    </div>
  </nav>

  <ul class="sidenav" id="opciones">
    <li><a href="ViewAspirante.php">Aspirantes</a></li>
    <li><a href="ViewCatalogo_cursos.php">Cursos</a></li>
    <li><a href="ViewAspirante_curso.php">Aspirantes-Cursos</a></li>
  </ul>