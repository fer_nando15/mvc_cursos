<!DOCTYPE html>
<html lang="en">
<head>
    <?php require "head.php"?>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php require "header.php"?>
    
    <!-- Modal Structure -->
    <div id="modal-aspirante-curso" class="modal">
        <form id="form-aspirante-curso">
            <div class="modal-content">
                <div class = "row">
                    <div class = "col s6">
                        <span>RFC</span>
                        <input id="iRFC" name = "iRFC" type="text" required >
                    </div>
                    <div class = "col s6">
                        <span>ID Curso</span>
                        <input id="iCurso" name = "iCurso" type="text" required >
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button href="#" id="enviar" type="submit" class="waves-effect waves-green btn-flat">Enviar</button>
                <button href="#" id="cancelar" class="modal-close waves-effect waves-green btn-flat">Cancelar</button>
            </div>
        </form>
    </div>

   

    <main>
    <div class="col s12">
            
            <button id="btnAgregar" class="right blue white-text waves-effect waves-teal btn-flat">Agregar Aspirante-Curso</button>
            <table id="tabla-aspirante-curso" class="table-responsive" width="100%">
                <thead>
                    <tr>
                        <th>RFC</th>
                        <th>CURSO</th>
                        <th>ACCIONES</th>
                    </tr>
                </thead>
            </table>
        </div>

    </main>
    <?php require "footer.php"?>
    <?php require "scripts.php"?>
    <script src="../assets/scripts/ViewASPCUR.js"></script>
</body>
</html>