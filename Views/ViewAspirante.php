<!DOCTYPE html>
<html lang="en">
<head>
    <?php require "head.php" ?>
    <title>Document</title>
</head>
<body>
    <?php require "header.php"?>
    <!-- Modal Structure -->
    <div id="modal-aspirantes" class="modal">
            <form id="form-aspirantes">
                <div class="modal-content">
                    <h6>FORMULARIO</h6>
                    <div class="row">
                        <div class="col s4">
                            <span>RFC</span>
                            <input id="iRFC"type="text" required >
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s4">
                            <span>Nombre</span>
                            <input id="iNombre" type="text" required >
                        </div>
                        <div class="col s4">
                            <span>Apellido Paterno</span>
                            <input id="iPaterno"type="text" required >
                        </div>
                        <div class="col s4">
                            <span>Apellido Materno</span>
                            <input id="iMaterno"type="text" required >
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s4">
                            <span>Empresa</span>
                            <input id="iEmpresa"type="text" required >
                        </div>
                        <div class="col s3">
                            <span>Telefono</span>
                            <input id="iTelefono"type="tel" required >
                        </div>
                        <div class="col s5">
                            <span>Email</span>
                            <input id="iEmail"type="email" required >
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button href="#" id="enviar" type="submit" class="waves-effect waves-green btn-flat">Enviar</button>
                    <button href="#" id="cancelar" class="modal-close waves-effect waves-green btn-flat">Cancelar</button>
                </div>
            </form>
        </div>
    <main>
        <div class="col s12">
            <button id="btnAgregar" class="right blue white-text waves-effect waves-teal btn-flat">Agregar Aspirante</button>
            <table id="tabla-aspirantes" class="table-responsive" width="100%">
                <thead>
                    <tr>
                        <th>RFC</th>
                        <th>Nombre</th>
                        <th>Apellido paterno</th>
                        <th>Apellido materno</th>
                        <th>Empresa</th>
                        <th>Telefono</th>
                        <th>Email</th>
                        <th>Fecha Registro</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
            </table>
        </div>
    </main>
    <?php require "footer.php"?>
    <?php require "scripts.php"?>
    <script src="../assets/scripts/ViewAspirante.js"></script>
</body>
</html>
    