<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="../assets/styles/materialize.min.css">
<link rel="stylesheet" href="../assets/styles/styles.css">
<link rel="stylesheet" href="../assets/styles/dataTables.css">
<link rel="stylesheet" href="../assets/styles/responsive.dataTables.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">